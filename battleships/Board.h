#pragma once
#include <utility>

using Point = std::pair<int, int>;

class Board
{
	void setOccupied(const Point& p, const Point& offset);
	bool isOccupied(const Point& p, const Point& offset);
	bool isOutOfBounds(const Point& p, const Point& offset);

	bool plane[10][10];
public:
	Board();
	~Board();

	bool tryToPlaceShip(const Point& p, const Point& offset);
};
