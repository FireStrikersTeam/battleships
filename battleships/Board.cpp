#include "Board.h"



Board::Board()
{
	for (int i = 0; i < 10; ++i)
		for (int j = 0; j < 10; ++j)
			plane[i][j] = false;
}


Board::~Board()
{
}

void Board::setOccupied(const Point& p, const Point& offset)
{
	for (int i=0; i<offset.first; ++i)
		plane[p.first + i][p.second] = true;
	for (int i = 0; i<offset.second; ++i)
		plane[p.first][p.second + i] = true;
}

bool Board::isOccupied(const Point& p, const Point& offset)
{
	for (int i = 0; i < offset.first; ++i)
		if (plane[p.first + i][p.second])
			return true;
	for (int i = 0; i<offset.second; ++i)
		if (plane[p.first][p.second + i]) 
			return true;

	return false;
}

bool Board::isOutOfBounds(const Point & p, const Point & offset)
{
	if (p.first < 0 || p.first > 9 || p.first + offset.first > 9
		|| p.second < 0 || p.second > 9 || p.second + offset.second > 9)
		return true;

	return false;
}

bool Board::tryToPlaceShip(const Point & p, const Point & offset)
{
	if (isOccupied(p, offset) || isOutOfBounds(p, offset))
		return false;

	setOccupied(p, offset);
	return true;
}
