#include <SFML/Window.hpp>
#include "Board.h"

int main()
{
	Board b;
	b.tryToPlaceShip({ 5,5 }, { 0,1 });

	b.tryToPlaceShip({ 5,5 }, { 0,2 });

	return 0;
}